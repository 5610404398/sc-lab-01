package model;

public class card {
	private int id;
	private int amount;
	private int balance;

	public card(int id) {
		this.id = id;
		this.balance = 0;
	}

	public void deposite(int amount) {
		this.balance += amount;
	}
	
	public void withdraw(int amount) {
		this.balance -= amount;
	}
	
	public String checkbalance() {
		return id + "  have " + balance + "  baht.";
	}
}
